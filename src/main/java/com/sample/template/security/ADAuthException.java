package com.sample.template.security;

import org.springframework.security.core.AuthenticationException;

public class ADAuthException extends AuthenticationException {
	private final String dataCode;

	ADAuthException(String dataCode, String message,
			Throwable cause) {
		super(message, cause);
		this.dataCode = dataCode;
	}

	public String getDataCode() {
		return dataCode;
	}
}