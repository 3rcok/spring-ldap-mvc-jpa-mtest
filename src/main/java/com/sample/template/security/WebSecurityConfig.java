package com.sample.template.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired

	private LDAPGrantedAuthoritiesMapper lDAPGrantedAuthoritiesMapper;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
//			.httpBasic().disable()
			.authorizeRequests()
			.anyRequest().fullyAuthenticated()
			.anyRequest().access("hasRole('ADMIN')")
				.antMatchers("/login").permitAll()
//				.antMatchers("/classesTable").hasRole("ADMIN")
			.and().formLogin()

//			.loginPage("/login")
//			.defaultSuccessUrl("/", true)
// 			.failureUrl("/login?error").permitAll()
//			.and().logout().permitAll()

				.loginPage("/login") // Specifies the login page URL
				.loginProcessingUrl("/signin") // Specifies the URL,which is used
				//in action attribute on the <from> tag
				.usernameParameter("username")  // Username parameter, used in name attribute
				// of the <input> tag. Default is 'username'.
				.passwordParameter("password")  // Password parameter, used in name attribute
				// of the <input> tag. Default is 'password'.
				.successHandler((req,res,auth)->{    //Success handler invoked after successful authentication
					if(auth.getAuthorities().isEmpty()){
						req.getSession().setAttribute("message", "access denied");
						res.sendRedirect("/login"); // Redirect user to index/home page
					} else {
						for (GrantedAuthority authority : auth.getAuthorities()) {
							System.out.println(authority.getAuthority());
						}
						System.out.println(auth.getName());
						res.sendRedirect("/"); // Redirect user to index/home page
					}
				})
//    .defaultSuccessUrl("/")   // URL, where user will go after authenticating successfully.
				// Skipped if successHandler() is used.
				.failureHandler((req,res,exp)->{  // Failure handler invoked after authentication failure
					String errMsg="";
					if(exp.getClass().isAssignableFrom(BadCredentialsException.class)){
						errMsg="Invalid username or password.";
					}else{
						errMsg="Unknown error - "+exp.getMessage();
					}
					req.getSession().setAttribute("message", errMsg);
					res.sendRedirect("/login"); // Redirect user to login page with error message.
				})
//    .failureUrl("/login?error")   // URL, where user will go after authentication failure.
				//  Skipped if failureHandler() is used.
				.permitAll() // Allow access to any URL associate to formLogin()
				.and()
				.logout()
				.logoutUrl("/signout")   // Specifies the logout URL, default URL is '/logout'
				.logoutSuccessHandler((req,res,auth)->{   // Logout handler called after successful logout
					req.getSession().setAttribute("message", "You are logged out successfully.");
					res.sendRedirect("/login"); // Redirect user to login page with message.
				})
//    .logoutSuccessUrl("/login") // URL, where user will be redirect after
				// successful logout. Ignored if logoutSuccessHandler() is used.
				.permitAll() // Allow access to any URL associate to logout()
				.and()
				.csrf().disable(); // Disable CSRF support
	}
//			;
//	}

	@Override

	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.ldapAuthentication()
				.userDnPatterns("uid={0},ou=people")
				.groupSearchBase("ou=groups")
				.authoritiesMapper(lDAPGrantedAuthoritiesMapper)
				.contextSource()
					.url("ldap://localhost:8389/dc=springframework,dc=org")
					.and()
				.passwordCompare()
				.passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder())
				.passwordAttribute("userPassword");

//				.passwordEncoder(new LdapShaPasswordEncoder())

	}

}
