package com.sample.template.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Set;

@Component
public class LDAPGrantedAuthoritiesMapper implements GrantedAuthoritiesMapper {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
    

    private String ADMIN_AD_ROLE = "ROLE_MANAGERS";
	
    @Override
    public Collection<? extends GrantedAuthority>  mapAuthorities(Collection<? extends GrantedAuthority> authorities) {
 
        Set<LDAPAuthority> roles = EnumSet.noneOf(LDAPAuthority.class);
 
        for (GrantedAuthority authority : authorities) {
        	
            if (ADMIN_AD_ROLE.equals(authority.getAuthority())) {
                roles.add(LDAPAuthority.ROLE_ADMIN);
                
                logger.debug("User has " + ADMIN_AD_ROLE + ". Added ADMIN role");
            }
        }
        return roles;
    }
}


