package com.sample.template.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * Maps LDAP Group application roles
 */
public enum LDAPAuthority implements GrantedAuthority {
    ROLE_ADMIN,
    ROLE_USER;

   
    public String getAuthority() {
        return name();
    }
   
}
