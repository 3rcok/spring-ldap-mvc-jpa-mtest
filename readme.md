# Spring Boot Project Skeleton with LDAP, MVC, JPA, Mockito and Junit

Spring Boot is the most popular framework to develop RESTful Services.

This is a Spring Boot project skeleton. You will be using Spring Boot, Maven (dependencies management), in memory LDAP, in memory database H2,Tomcat Embedded Web Server, MVC, JPA, formlogin and Mockito/Junit test.
You can change configurations to connect to real LDAP and database server.

### Recommended Tools
- Java 8 or Later
- Intellij IDEA
- Spring Boot 2.0.5.RELEASE or Later
- Spring Security 5.0

### Prerequisites
http://help.github.com/set-up-git-redirect[Git] and the http://www.oracle.com/technetwork/java/javase/downloads[JDK8 build].

Be sure that your `JAVA_HOME` environment variable points to the `jdk1.8.0` folder extracted from the JDK download.

### Check out sources

git clone https://3rcok@bitbucket.org/3rcok/spring-ldap-mvc-jpa-mtest.git

### Install and Run Tests

mvn clean install

### Run Application

mvn spring-boot:run

### Check In Browser

The main focus of this project is unit testing. The links below is for basic functional testing.  
http://localhost:8080  

To validate LDAP users  
user: ben password: password Role: ADMIN  
user: bob password: password Role: USER  

The security configuration only allows valid ADMIN user to access secure content. So only ADMIN user "ben" can access http://localhost:8080/secure after login.  
The other not ADMIN user "bob" will get access denied.


### Covered Test Cases

The Spring Boot applicaion has multiple layers, such as Authentication, REST API, Controller, Service, Repository...   
This project covers tests for each layers, and has intergration tests for all the layers involved.  
For LDAP, it reads users from test-server.ldif under resources. For test data in the database, it reads items from data.sql under resources.  

### TODO List

Audit  
Page Query





